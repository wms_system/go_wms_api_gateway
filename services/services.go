package services

import (
	"go_wms_api_gateway/config"
	"go_wms_api_gateway/genproto/warehouse_service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	Branch() warehouse_service.BranchServiceClient
}

type grpcClients struct {
	branch warehouse_service.BranchServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {
	//Warehouse Service ...
	connWarehouseService, err := grpc.Dial(
		cfg.WareHouseServiceHost+cfg.WareHouseGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		return nil, err
	}

	return &grpcClients{
		branch: warehouse_service.NewBranchServiceClient(connWarehouseService),
	}, nil

}

func (g *grpcClients) Branch() warehouse_service.BranchServiceClient {
	return g.branch
}
